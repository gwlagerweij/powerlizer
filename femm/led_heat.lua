-- Parameters
W = 10;              -- Width [cm]
H = 17.5;            -- Length [cm]

Wled = 0.36;         -- Width of LED [cm]
Hled = 0.18;         -- Height of LED [cm]
Rled = 24;           -- LED thermal resistance [K/W]
Ta   = 45 + 273.13;  -- Ambient temperature [K]

 -- Calculate effective k for LED [W/(m K)]
kled = Hled / (Rled * Wled * H) * 100 * 6; 

 -- Calculate effective LED power [W]
eta = 2.6;           -- Efficiency [%]
P   = 0.18;          -- Total power [W]
led_power = (100 - eta) / 100 * P * 6;

-- Problem definition
newdocument(2);
hi_probdef("centimeters", "planar", 1e-8, H, 30);

Nlayers   = 4;
thickness = {35e-4, 0.157, 35e-4, 0.1};
width     = {W, W, W, W};
mat       = {"Copper", "FR4", "Copper", "Teflon"};
cond      = {nil, nil, nil, nil};

-- Material definition
hi_addmaterial("Copper", 401, 401, 0, 3.41);
hi_addtkpoint("Copper", 273, 401)
hi_addtkpoint("Copper", 473, 389)
hi_addtkpoint("Copper", 673, 378)
hi_addtkpoint("Copper", 873, 366)
hi_addtkpoint("Copper", 1073, 352)
hi_addtkpoint("Copper", 1273, 336)

hi_addmaterial("FR4", 0.9, 0.3, 0, 0);
hi_addmaterial("LED", kled, kled, 0, 0);
hi_addmaterial("Teflon", 0.35, 0.35, 0, 1.8);

hi_addconductorprop("LED1", 0, led_power, 0);
hi_addconductorprop("LED2", 0, led_power, 0);
hi_addconductorprop("LED3", 0, led_power, 0);
hi_addconductorprop("LED4", 0, led_power, 0);
hi_addboundprop("convection", 2, 0, 0, Ta, 15, 0);

-- Draw the geometry
curY = 0;
for i = 1, Nlayers do
	W = width[i];
	H = thickness[i];
	M = mat[i];
	C = cond[i];
	nextY = curY - H;
	
	hi_addnode(-W/2, curY);
	hi_addnode(W/2, curY);
	hi_addnode(-W/2, nextY);
	hi_addnode(W/2, nextY);
	
	hi_addsegment(-W/2, curY, W/2, curY);
	hi_addsegment(W/2, curY, W/2, nextY);
	hi_addsegment(W/2, nextY, -W/2, nextY);
	hi_addsegment(-W/2, nextY, -W/2, curY);
	
	hi_addblocklabel(0, (curY + nextY) / 2);
	hi_selectlabel(0, (curY + nextY) / 2);
	hi_setblockprop(M, 1, 0, 0);
	
	if(C ~= nil)
	then
		hi_selectsegment(-W/2, curY, W/2, curY);
		hi_setsegmentprop("", 0, 1, 0, 0, C);
	end
	
	curY = nextY;
end

hi_selectsegment(-W/2, curY, W/2, curY);
hi_setsegmentprop("convection", 0, 1, 0, 0, "");

-- Draw LEDs
dW = 1.4; -- Distance from border
leds = {"LED1", "LED2", "LED3", "LED4"};
for i = 1, 4 do
	x = -W/2 + dW + (i - 1) * (W - 2 * dW) / 3
	
	hi_addnode(x - Wled / 2, 0);
	hi_addnode(x + Wled / 2, 0);
	hi_addnode(x - Wled / 2, Hled);
	hi_addnode(x + Wled / 2, Hled);
	
	hi_addsegment(x - Wled / 2, 0, x + Wled / 2, 0)
	hi_addsegment(x + Wled / 2, 0, x + Wled / 2, Hled)
	hi_addsegment(x - Wled / 2, Hled, x + Wled / 2, Hled)
	hi_addsegment(x - Wled / 2, 0, x - Wled / 2, Hled)
	
	hi_addblocklabel(x, Hled / 2);
	hi_selectlabel(x, Hled / 2);
	hi_setblockprop("LED", 1, 0, 0);
	
	hi_selectsegment(x - Wled / 2, Hled, x + Wled / 2, Hled);
	hi_setsegmentprop("", 0, 1, 0, 0, leds[i]);
end

-- Set up view
hi_clearselected();
hi_zoomnatural();

-- Save and analyze
hi_saveas("led_heat.feh");
hi_analyze();

-- Load solution
hi_loadsolution();