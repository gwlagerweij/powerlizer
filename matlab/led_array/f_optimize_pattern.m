% F_OPTIMIZE_PATTERN Optimize LED pattern for a given Ncol, Nrow
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   04/05/2021
%
%  [dW, dH, Emin] = f_optimize_pattern(setup, Ncol, Nrow, pat);        (1)
%  [dW, dH, Emin] = f_optimize_pattern(setup, Ncol, Nrow, pat, plot);  (2)
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                        | Unit  | Size  | Type   |
% ========================================================================
% | setup  | Structure describing the problem   | [-]   | 1     | Struct |
% ------------------------------------------------------------------------
% | Ncol   | Number of columns                  | [-]   | 1     | Double |
% ------------------------------------------------------------------------
% | Nrow   | Number of rows                     | [-]   | 1     | Double |
% ------------------------------------------------------------------------
% | pat    | Pattern name                       | [-]   | 1 x N | Char   |
% ------------------------------------------------------------------------
% | plot   | Show optimization plot             | [-]   | 1     | Bool   |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                        | Unit  | Size  | Type   |
% ========================================================================
% | dW     | Distance from wall (x)             | [m]   | 1     | Double |
% ------------------------------------------------------------------------
% | dH     | Distance from wall (y)             | [m]   | 1     | Double |
% ------------------------------------------------------------------------
% | Emin   | Maximized irradiance               | [-]   | 1     | Double |
% ------------------------------------------------------------------------
function [dW, dH, Emin] = f_optimize_pattern(setup, Ncol, Nrow, pat, plot)
    % Show debug information if plot == 1
    if(nargin == 5 && plot == 1)
        options = optimset('Display', 'iter', 'PlotFcns', @optimplotfval);
    else
        options = optimset('Display', 'off');
    end
    
    % Multi-variable optimization with fminsearch
    f = @(x) (objfunc(x, setup, Ncol, Nrow, pat));
    [x, fval] = fminsearch(f, [1.5e-2, 1.5e-2], options);
    
    % Optimization results
    dW   = x(1); dH = x(2);
    Emin = -fval;
end

% OBJFUNC Objective function to minimize
function [v] = objfunc(x, setup, Ncol, Nrow, pattern)
    % Generate pattern with optimization variables x = [dW, dH]
    [setup.src_pos, setup.N] = f_gen_led_positions(Ncol, Nrow, setup.W, setup.H, x(1), x(2), pattern);
    
    % Calculate irradiance for the generated pattern
    %  and return the overdose
    [~, ~, E, ~] = f_calc_irradiance(setup);
    v = -min(min(E));
end