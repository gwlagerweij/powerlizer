%% Radiation Pattern
theta = linspace(-pi / 2, pi / 2, 200);
Irel_ = f_gen_rad_pattern('SUCULBN1_V2');
Irel  = Irel_(theta);

%% Plot
figure(1); clf
plot(theta / pi * 180, Irel, 'k')

xlim([-90, 90])
grid on

figure(2); clf
polarplot(theta, Irel, 'k');
hold on
% polarplot([-pi/3, -pi/3], [0, 1.1], 'k--')
% polarplot([pi/3, pi/3], [0, 1.1], 'k--')
% th = linspace(-pi/3, pi/3, 100);
% polarplot(th, 0.5 * ones(1, length(th)), 'k--')
thetalim([-90, 90])
rlim([0, 1.1])

ax = gca;
set(ax, 'ThetaZeroLocation', 'top')
ax.ThetaAxis.Label.String   = '\theta [deg]';
ax.ThetaAxis.Label.Position = [45, 1.4, 0];
ax.ThetaAxis.Direction      = 'reverse';

ax.RAxis.Label.String = 'I_{rel}(\theta)';

fig = gcf;
set(fig, 'Units', 'centimeters')
set(fig, 'Position', [1, 1, 7.7 * 0.85, 5.5 * 0.85])