% OPTIMIZE_LAYOUT Optimize LED array layout
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   04/05/2021
%
%% Parameters
setup.W   = 10e-2;        % Width of disinfection area
setup.H   = 17.5e-2;      % Height of disinfection area
setup.z   = 2e-2;         % 
setup.Nel = 100;          % Number of grid elements

setup.Gamma = 0.9;        % Wall reflectivity

led_name = 'SUCULBN1_V2'; % Name of the LED

%%
pattern = {'rectangle', 'hexagon', 'diagonal', 'diamond'};
Ncol    = [3, 4, 5];
Nrow    = [4, 5, 6];

%% Generate LED radiation pattern
[setup.Irel, setup.Phi_e, setup.Omega] = f_gen_rad_pattern(led_name);

%% Optimize solutions
data = [];
data_idx = [];
for i = 1:length(pattern)
    fprintf('Starting pattern %s\n', pattern{i});
    for j = 1:length(Ncol)
        for k = 1:length(Nrow)
            [dW, dH, Emin] = f_optimize_pattern(setup, Ncol(j), Nrow(k), pattern{i});
            
            data = [data; {pattern(i)}, Ncol(j), Nrow(k), Emin, dW, dH];
            data_idx = [data_idx; i];
        end
    end
end

%% Put data in table
headers = {'pattern', 'Ncol', 'Nrow', 'Emin', 'dW', 'dH'};
T = cell2table(data);
T.Properties.VariableNames = headers;