% F_GEN_RAD_PATTERN Generate LED radiation pattern
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   01/05/2021
%
%  [Irel, Phi_e, Omega] = f_gen_rad_pattern(led_name);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name     | Description                      | Unit  | Size  | Type   |
% ========================================================================
% | led_name | Name of the LED                  | [-]   | 1 x N | Char   |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name     | Description                      | Unit  | Size  | Type   |
% ========================================================================
% | Irel     | Relative radiant intensity       | [-]   | 1     | Handle |
% ------------------------------------------------------------------------
% | Phi_e    | Radiant flux                     | [W]   | 1     | Double |
% ------------------------------------------------------------------------
% | Omega    | Solid angle                      | [sr]  | 1     | Double |
% ------------------------------------------------------------------------
function [Irel, Phi_e, Omega] = f_gen_rad_pattern(led_name)
    % Load LED parameters
    load(sprintf('leds/%s.mat', led_name), 'Irel_pat', 'theta_pat', 'Phi_e'); 
    
    % Create handle to calculate relative radiant intensity
    Irel = @(theta) (f_calc_irel(Irel_pat, theta_pat, theta));
    
    % Calculate solid angle
    theta = linspace(-pi / 2, pi / 2, 1000);
    Omega = 2 * pi * trapz(theta, Irel(theta) .* cos(theta));
end

% F_CALC_IREL Function to be used as handle
%  Calculates relative radiant intensity at angle theta
%   from a set of data points extracted from the datasheet.
function Irel = f_calc_irel(Irel_pat, theta_pat, theta)
    % Interpolate datasheet values
    Irel = interp1(theta_pat, Irel_pat, abs(theta));
    
    % If no value is available for Irel at the given theta, return zero
    % This can occur if theta is outside the range of theta_pat
    Irel(isnan(Irel)) = 0;
end