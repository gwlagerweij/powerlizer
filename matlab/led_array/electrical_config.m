Nled = 24;
Npar = 1:Nled;
Nser = nan(size(Npar));

Vlo = 12.5;
Vhi = 21;

Vf = 6;
sigma_Vf = 0.5;

%% Find possible configurations
for i = 1:length(Npar)
    N = Nled / Npar(i);
    if(mod(N, 1) == 0)
        Nser(i) = N;
    end
end

%%
figure(1); clf
hold on

patch([0, max(Npar), max(Npar), 0], [0, 0, Vlo, Vlo], 'g', 'EdgeColor', 'none', 'FaceAlpha', 0.3)
patch([0, max(Npar), max(Npar), 0], [Vlo, Vlo, Vhi, Vhi], 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.3)
patch([0, max(Npar), max(Npar), 0], [Vhi, Vhi, 40, 40], 'g', 'EdgeColor', 'none', 'FaceAlpha', 0.3)
patch([0, max(Npar), max(Npar), 0], [40, 40, max(Nser * (Vf + sigma_Vf)), max(Nser * (Vf + sigma_Vf))], 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.3)

plot([0, max(Npar)], [Vlo, Vlo], 'k--')
plot([0, max(Npar)], [Vhi, Vhi], 'k--')
plot([0, max(Npar)], [40, 40], 'k--')

e = errorbar(Npar, Nser * Vf, Nser * sigma_Vf, 'k');
e.LineStyle = 'none';

xlabel('Number of Parallel Branches')
ylabel('Forward Voltage [V]')

xlim([0, max(Npar)])
ylim([0, max(Nser * (Vf + sigma_Vf))])