% IRRADIANCE Simulate and plot irradiance
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   30/04/2021
%
%% Parameters
setup.W   = 10e-2;        % Width of disinfection area
setup.H   = 17.5e-2;      % Height of disinfection area
setup.z   = 2e-2;         % Distance of LEDs to disinfection area
setup.Nel = 200;          % Number of grid elements

setup.Gamma = 0.9;        % Wall reflectivity

led_name = 'SUCULBN1_V2'; % Name of the LED
pattern  = 'rectangle';   % LED placement pattern

Ncol = 4;                 % Number of columns of LEDs
Nrow = 6;                 % Number of rows of LEDs
dW   = 1.4e-2;            % x distance from wall
dH   = 1.35e-2;           % y distance from wall

%% Calculate irradiance
[setup.src_pos, setup.N] = f_gen_led_positions(Ncol, Nrow, setup.W, setup.H, dW, dH, pattern);
[setup.Irel, setup.Phi_e, setup.Omega] = f_gen_rad_pattern(led_name);
[x, y, E, O] = f_calc_irradiance(setup);
Emin = min(min(E)) * 0.1;

%% Plot
figure(1); clf

% Plot irradiance
s = surface(y * 1e2, x * 1e2, E' * 1e-1);
s.EdgeAlpha = 0;
%contourf(y * 1e2, x * 1e2, E' * 1e-1);
xlabel('y [cm]')
ylabel('x [cm]')

% Scale colour [0, Emax]
c = gca;
c.CLim = [0, max(max(E)) * 0.1];

cb = colorbar;
cb.Label.String = 'Irradiance [mW/cm^2]';

% Draw LEDs
hold on
Wled = 0.36; % Width [cm]
Hled = 0.36; % Height [cm]
for i = 1:length(setup.src_pos)
    x_ = setup.src_pos(1, i) * 1e2;
    y_ = setup.src_pos(2, i) * 1e2;
    plot3([y_ - 0.5 * Hled, y_ - 0.5 * Hled, y_ + 0.5 * Hled, y_ + 0.5 * Hled, y_ - 0.5 * Hled, y_ - 0.5 * Hled],...
          [x_ - 0.5 * Wled, x_ + 0.5 * Wled, x_ + 0.5 * Wled, x_ - 0.5 * Wled, x_ - 0.5 * Wled, x_ + 0.5 * Wled],...
          [5, 5, 5, 5, 5, 5], 'k');
end
axis equal
xlim([0, setup.H] * 1e2)
ylim([0, setup.W] * 1e2)

box on