pos1 = [3.5, 2.5];
pos2 = [2, 0.5];

r1 = pos1(1); r2 = pos2(1);
dz = abs(pos1(2) - pos2(2));

h1 = dz * r1 / (r1 + r2);
h2 = dz * r2 / (r1 + r2);

theta = atan((r1 + r2) / dz);
theta = theta / pi * 180;

plot([pos1(1), 0, pos2(1)], [pos1(2), pos1(2) - h1, pos2(2)]);
xlim([0, 5])
ylim([0, 3])