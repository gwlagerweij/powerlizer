clear
load('optimization_results/optim_2021_05_07.mat')

marker = ['s', 'o', 'x', 'd'];
color  = {[0 0.4470 0.7410], [0.8500 0.3250 0.0980], [0.4940 0.1840 0.5560], [0.9290 0.6940 0.1250]};

%% Plot
figure(1); clf
t = tiledlayout(1,2);
t.TileSpacing = 'compact';
t.Padding = 'compact';
for i = 1:length(pattern)
    idx  = (data_idx == i);
    Emin = cell2mat(data(idx, 4));
    Nled = nan(size(Emin));
    O    = nan(size(Emin));
   
    idx = find(idx);
    for j = 1:length(idx)
        [setup.src_pos, setup.N] = f_gen_led_positions(data{j, 2}, data{j, 3}, setup.W, setup.H, data{j, 5}, data{j, 6}, pattern{i});
        [~, ~, ~, O_] = f_calc_irradiance(setup);
        Nled(j) = setup.N;
        O(j) = O_;
    end
    
    nexttile(1)
    hold on
    if(marker(i) == 'x')
        scatter(O, Nled, 20, color{i}, marker(i));
    else
        scatter(O, Nled, 20, color{i}, marker(i), 'filled');
    end
    
    nexttile(2)
    hold on
    if(marker(i) == 'x')
        scatter(Emin * 1e2, Nled, 20, color{i}, marker(i));
    else
        scatter(Emin * 1e2, Nled, 20, color{i}, marker(i), 'filled');
    end
end

%% Axis labels, limits, legend
nexttile(1)
xlabel('Overdose')
ylabel('Number of LEDs')
grid on
ax1 = gca;

%xlim([1.0, 1.75])

nexttile(2)
xlabel('E_{min} [\muW/cm^2]')
legend(pattern, 'Location', 'NorthEastOutside')
grid on
ax2 = gca;

lim = ax2.XLim;
Emmin = 167;
%xlim([Emmin, lim(2)])

linkaxes([ax1, ax2], 'y')

f = gcf;
f.Units = 'centimeters';
f.Position = [1, 1, 15.5, 7];