% F_CALC_IRRADIANCE Calculate irradiance on a rectangular domain
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   30/04/2021
%
%  [x, y, E, O] = f_calc_irradiance(setup);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | setup  | Structure describing the problem | [-]     | 1     | Struct |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | x      | x coordinate array               | [m]     | 1 x N | Double |
% ------------------------------------------------------------------------
% | y      | y coordinate array               | [m]     | 1 x N | Double |
% -----------------------------------------------------------------------
% | E      | Irradiance                       | [W/m^2] | N x N | Double |
% -----------------------------------------------------------------------
% | O      | Overdose                         | [-]     | 1     | Double |
% ------------------------------------------------------------------------
function [x, y, E, O] = f_calc_irradiance(setup)
    % Grid elements
    x = linspace(0, setup.W, setup.Nel);
    y = linspace(0, setup.H, setup.Nel)';
    z = setup.z;
    
    % Element position matrix
    el_pos(:,:,1) = repmat(x, size(y));
    el_pos(:,:,2) = repmat(y, size(x));
    el_pos(:,:,3) = z * ones(length(y), length(x));
    src_pos       = setup.src_pos;
    
    % Wall definition
    wall = [{'x', 0}; {'x', setup.H}; {'y', 0}; {'y', setup.W}];
    
    % Calculate irradiance
    E = zeros(setup.Nel);
    for i = 1:setup.N
        src = src_pos(:, i);
        
        % Direct incidence
        r = sqrt((x - src(1)).^2 + (y - src(2)).^2 + (z - src(3)).^2);
        theta = acos((z - src(3)) ./ r);
        E = E + setup.Phi_e * setup.Irel(theta) .* cos(theta) ./ (r.^2 * setup.Omega);
        
        % First reflection
        for j = 1:size(wall, 1)
            [theta, r] = f_calc_reflection(src, el_pos, wall{j, 1}, wall{j, 2});
            E = E + setup.Gamma * setup.Phi_e * setup.Irel(theta) .* cos(theta) ./ (r.^2 * setup.Omega);
        end
    end
    
    % Calculate overdose
    O = max(max(E)) / min(min(E));
end