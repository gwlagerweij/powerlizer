% F_GEN_LED_POSITIONS Generate LED position vector
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   29/04/2021
%
%  [pos, N] = f_gen_led_positions(Ncol, Nrow, W, H, dW, dH, method);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                        | Unit  | Size  | Type   |
% ========================================================================
% | Ncol   | Number of columns                  | [-]   | 1     | Double |
% ------------------------------------------------------------------------
% | Nrow   | Number of rows                     | [-]   | 1     | Double |
% ------------------------------------------------------------------------
% | W      | Width                              | [m]   | 1     | Double |
% ------------------------------------------------------------------------
% | H      | Height                             | [m]   | 1     | Double |
% ------------------------------------------------------------------------
% | dW     | Distance from edge (along x-axis)  | [m]   | 1     | Double |
% ------------------------------------------------------------------------
% | dH     | Distance from edge (along y-axis)  | [m]   | 1     | Double |
% ------------------------------------------------------------------------
% | method | Generation method                  | [-]   | 1 x M | Char   |
% |        |  rectangle, hexagon,               |       |       |        |
% |        |  diagonal, diamond                 |       |       |        |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                        | Unit  | Size  | Type   |
% ========================================================================
% | pos    | Generated positions                | [m]   | 3 x N | Double |
% ------------------------------------------------------------------------
% | N      | Number of generated positions      | [-]   | 1     | Double |
% ------------------------------------------------------------------------
function [pos, N] = f_gen_led_positions(Ncol, Nrow, W, H, dW, dH, method)
    % Select correct generation function based on the selected method
    if(strcmpi(method, 'rectangle'))
        [pos, N] = f_gen_led_pos_rectangle(Ncol, Nrow, W, H, dW, dH);
    elseif(strcmpi(method, 'diagonal'))
        [pos, N] = f_gen_led_pos_diagonal(Ncol, Nrow, W, H, dW, dH);
    elseif(strcmpi(method, 'diamond'))
        [pos, N] = f_gen_led_pos_diamond(Ncol, Nrow, W, H, dW, dH);
    elseif(strcmpi(method, 'hexagon'))
        [pos, N] = f_gen_led_pos_hexagon(Ncol, Nrow, W, H, dW, dH);
    else
        warning('Unknown generation method: %s.\nReturning empty position vector.', method);
        pos = [];
        N   = 0;
    end
end

% Generate positions in a rectangle pattern
%   *   *   *
%   *   *   *
%   *   *   *
function [pos, N] = f_gen_led_pos_rectangle(Ncol, Nrow, W, H, dW, dH)
    N = Ncol * Nrow;
    pos = nan(3, N);
    for i = 1:Ncol
        for j = 1:Nrow
            pos(1, (i - 1) * Nrow + j) = (i - 1) * (W - 2 * dW) / (Ncol - 1) + dW;
            pos(2, (i - 1) * Nrow + j) = (j - 1) * (H - 2 * dH) / (Nrow - 1) + dH;
            pos(3, (i - 1) * Nrow + j) = 0;
        end
    end
end

% Generate positions in a diagonal pattern
%   *   *   *
%     *   *
%   *   *   *
function [pos, N] = f_gen_led_pos_diagonal(Ncol, Nrow, W, H, dW, dH)
    pos = [];
    for i = 1:Ncol
        if(mod(i, 2) == 1) % Generate odd columns like 'rectangle'
            for j = 1:Nrow
                x0 = (i - 1) * (W - 2 * dW) / (Ncol - 1) + dW;
                y0 = (j - 1) * (H - 2 * dH) / (Nrow - 1) + dH;
                z0 = 0;
                pos = [pos, [x0; y0; z0]];
            end
        else               % Generate even columns with an offset
            for j = 1:Nrow - 1
                x0 = (i - 1) * (W - 2 * dW) / (Ncol - 1) + dW;
                y0 = (j - 0.5) * (H - 2 * dH) / (Nrow - 1) + dH;
                z0 = 0;
                pos = [pos, [x0; y0; z0]];
            end
        end
    end
    N = size(pos, 2);
end

% Generate positions in a diamond pattern
%    *   *   *
%  *   *   *   *
%    *   *   *
function [pos, N] = f_gen_led_pos_diamond(Ncol, Nrow, W, H, dW, dH)
    pos = [];
    for i = 1:Ncol
        if(mod(i, 2) == 0) % Generate even columns like 'rectangle'
            for j = 1:Nrow
                x0 = (i - 1) * (W - 2 * dW) / (Ncol - 1) + dW;
                y0 = (j - 1) * (H - 2 * dH) / (Nrow - 1) + dH;
                z0 = 0;
                pos = [pos, [x0; y0; z0]];
            end
        else               % Generate odd columns with an offset
            for j = 1:Nrow - 1
                x0 = (i - 1) * (W - 2 * dW) / (Ncol - 1) + dW;
                y0 = (j - 0.5) * (H - 2 * dH) / (Nrow - 1) + dH;
                z0 = 0;
                pos = [pos, [x0; y0; z0]];
            end
        end
    end
    N = size(pos, 2);
end

% Generate positions in a hexagon pattern
%     *   *   *   
%   *   *   *   *
%     *   *   *
%   *   *   *   *
%     *   *   *
function [pos, N] = f_gen_led_pos_hexagon(Ncol, Nrow, W, H, dW, dH)
    pos = [];
    for j = 1:Nrow
        if(mod(j, 2) == 0) % Generate odd rows like 'rectangle'
            for i = 1:Ncol
                x0 = (i - 1) * (W - 2 * dW) / (Ncol - 1) + dW;
                y0 = (j - 1) * (H - 2 * dH) / (Nrow - 1) + dH;
                z0 = 0;
                pos = [pos, [x0; y0; z0]];
            end
        else               % Generate even rows with an offset
            for i = 1:Ncol - 1
                x0 = (i - 0.5) * (W - 2 * dW) / (Ncol - 1) + dW;
                y0 = (j - 1) * (H - 2 * dH) / (Nrow - 1) + dH;
                z0 = 0;
                pos = [pos, [x0; y0; z0]];
            end
        end
    end
    N = size(pos, 2);
end