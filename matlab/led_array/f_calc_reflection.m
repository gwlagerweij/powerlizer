% F_CALC_REFLECTION Calculate angle and travel distance of reflection
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   30/04/2021
%
%  [theta, r] = f_calc_reflection(pos1, pos2, wall, coord);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                   | Unit   | Size      | Type   |
% ========================================================================
% | pos1   | Source position               | [m]    | 1 x 3     | Double |
% ------------------------------------------------------------------------
% | pos2   | Target position               | [m]    | N x M x 3 | Double |
% ------------------------------------------------------------------------
% | wall   | Wall type ('x' or 'y')        | [-]    | 1         | Char   |
% ------------------------------------------------------------------------
% | coord  | Wall coordinate               | [m]    | 1         | Double |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                   | Unit   | Size      | Type   |
% ========================================================================
% | theta  | Incidence angle               | [rad]  | N x M     | Double |
% ------------------------------------------------------------------------
% | r      | Travel distance               | [m]    | N x M     | Double |
% ------------------------------------------------------------------------
function [theta, r] = f_calc_reflection(pos1, pos2, wall, coord)
    % Calculate distance to wall perpendicular to wall axis
    if(strcmpi(wall, 'x'))
        r1 = abs(pos1(2) - coord);
        r2 = abs(pos2(:, :, 2) - coord);
        dx = abs(pos1(1) - pos2(:, :, 1));
    elseif(strcmpi(wall, 'y'))
        r1 = abs(pos1(1) - coord);
        r2 = abs(pos2(:, :, 1) - coord);
        dx = abs(pos1(2) - pos2(:, :, 2));
    else
        error('Invalid wall: %s', wall);
    end
    
    % Calculate distance along wall axis and z-axis
    dz = abs(pos1(3) - pos2(:, :, 3));
    
    % Solve for unknown distances
    r0 = r1 + r2;
    d1 = dx .* r1 ./ r0;
    d2 = dx .* r2 ./ r0;
    h1 = dz .* r1 ./ r0;
    h2 = dz .* r2 ./ r0;
    
    % Calculate incidence angle and total travel distance
    theta = pi / 2 - atan(dz ./ r0);
    r = sqrt(d1.^2 + h1.^2 + r1.^2) + sqrt(d2.^2 + h2.^2 + r2.^2);
end