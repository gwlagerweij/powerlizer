% F_LOAD_SWITCH Load switch model from component database
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   19/05/2021
%
%  [Rson, tson, tsoff, Cso] = f_load_switch(name)
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | name   | Component name                   | [-]     | 1 x N | Char   |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | Rson   | On-state resistance              | [Ohm]   | 1     | Double |
% ------------------------------------------------------------------------
% | tson   | Turn-on time                     | [s]     | 1     | Double |
% ------------------------------------------------------------------------
% | tsoff  | Turn-off time                    | [s]     | 1     | Double |
% ------------------------------------------------------------------------
% | Cso    | Output capacitance               | [F]     | 1     | Double |
% ------------------------------------------------------------------------
function [Rson, tson, tsoff, Cso] = f_load_switch(name)
    data = load(sprintf('components/switches/%s.mat', name));
    Rson  = data.Rson;
    tson  = data.tson;
    tsoff = data.tsoff;
    Cso   = data.Cso;
end