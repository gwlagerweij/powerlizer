%%
Vi  = linspace(12.5, 21, 100)';
Vo  = linspace(33, 39, 200);
Io  = 120e-3;
L   = 100e-6;
C   = 4.7e-6;
fsw = 750e3;

%%
D = 1 - Vi ./ Vo;
Iav = Io .* Vo ./ Vi;

Iripp = Vi .* D ./ (L * fsw);
Irp = Iripp ./ Iav * 100;

Vripp = Io .* D ./ (C * fsw);

min(min(Vripp))
max(max(Vripp))

%%
figure(1)
contourf(Vi, Vo, Iav')

figure(2)
contourf(Vi, Vo, Iripp')

figure(3)
contourf(Vi, Vo, Irp')

figure(4)
contourf(Vi, Vo, Vripp')

spread(3, 2)