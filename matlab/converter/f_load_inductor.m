% F_LOAD_INDUCTOR Load inductor model from component database
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   14/05/2021
%
%  [L, Rdc, Rcore0, Rac0] = f_load_inductor(name)
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | name   | Component name                   | [-]     | 1 x N | Char   |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                   | Unit       | Size  | Type   |
% ========================================================================
% | L      | Inductance                    | [H]        | 1     | Double |
% ------------------------------------------------------------------------
% | Rdc    | DC winding resistance         | [Ohm]      | 1     | Double |
% ------------------------------------------------------------------------
% | Rcore0 | Core loss model coefficient   | [Ohm/rtHz] | 1     | Double |
% ------------------------------------------------------------------------
% | Rac0   | AC resistance model coeff     | [Ohm/rtHz] | 1     | Double |
% ------------------------------------------------------------------------
function [L, Rdc, Rcore0, Rac0] = f_load_inductor(name)
    data = load(sprintf('components/inductors/%s.mat', name));
    L      = data.L;
    Rdc    = data.Rdc;
    Rcore0 = data.Rcore0;
    Rac0   = data.Rac0;
end