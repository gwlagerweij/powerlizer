%%
Vi  = linspace(12.5, 21, 100)';
Vo  = linspace(33, 39, 200);
Io  = 120e-3;
L   = 12e-6;
fsw = 750e3;

C   = 2.6e-6;
Cesr = 1.8e-3;

%%
eta = 1;
D   = 1 - eta * Vi ./ Vo;                     % CCM duty cycle
Vrippccm = Io .* D ./ (C * fsw);

[Iav, Iripp, D, d, DCM] = f_calc_op(Vi, Vo, Io, L, fsw, eta);
Irp   = Iripp ./ Iav * 100;

Vripp  = (Iripp - Io).^2 .* Io * L ./ (C * Iripp .* Vi .* D) + (Iripp - Io) .* Cesr;

Cmin = (Iripp - Io).^2 .* Io * L ./ (50e-3 * Iripp .* Vi .* D);

%%
figure(1)
hold on
contourf(Vi, Vo, Vripp')
colorbar

%contourf(Vi, Vo, Iav')
% 
% figure(2)
% contourf(Vi, Vo, Iripp')
% 
% figure(3)
% contourf(Vi, Vo, Irp')

spread(3, 2)