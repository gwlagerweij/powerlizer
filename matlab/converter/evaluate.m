%%
Vi  = linspace(12.5, 21, 100)';
Io  = 120e-3;
Vo  = [33, 39];
fsw = 750e3;

%%
inductor = 'XGL4030-123'; 'LPS4018-103';
diode    = 'PMEG6010CEH';
switch_  = 'LT3599';

[Ploss, Pd, Ps, PLcond, PLcore, Psw, mode] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_);
Ploss = Ploss + 380e-3 + 0.77 * Io;

Po  = (Vo - 0.77) * Io;
Pi  = Po + Ploss;
eta = Po ./ Pi * 100;

%%
figure(1)
plot(Vi, Ploss * 1e3)

grid on
%grid minor

leg = sprintf('V_{out} = %.0f Vend', Vo);
leg = split(leg, 'end');
leg = leg(1:end-1);
legend(leg, 'location', 'southwest')

xlabel('Input Voltage [V]')
ylabel('Total Power Loss [mW]')

figure(2)
plot(Vi, eta)
grid on
grid minor

spread(3, 2)