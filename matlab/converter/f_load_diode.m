% F_LOAD_DIODE Load diode model from component database
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   14/05/2021
%
%  [Ron, Vsat, Co] = f_load_diode(name);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | name   | Component name                   | [-]     | 1 x N | Char   |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | Ron    | On-state resistance              | [Ohm]   | 1     | Double |
% ------------------------------------------------------------------------
% | Vsat   | On-state saturation voltage      | [V]     | 1     | Double |
% ------------------------------------------------------------------------
% | Co     | Output capacitance               | [F]     | 1     | Double |
% ------------------------------------------------------------------------
function [Ron, Vsat, Co] = f_load_diode(name)
    data = load(sprintf('components/diodes/%s.mat', name));
    Ron  = data.Ron;
    Vsat = data.Vsat;
    Co   = data.Co;
end