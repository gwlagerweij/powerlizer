% F_CALC_LOSS Calculate power loss of the converter
%   Uses either an efficiency estimate or will iteratively calculate loss
%   until it converges to an efficiency.
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   14/05/2021
%
%  [Ploss, Pd, Ps, PLcond, PLcore, Pssw, mode] 
%    = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_);         (1)
%  [Ploss, Pd, Ps, PLcond, PLcore, Pssw, mode] 
%    = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_, eta);    (2)
%
% Inputs:
% ------------------------------------------------------------------------
% | Name     | Description                    | Unit    | Size  | Type   |
% ========================================================================
% | Vi       | Input voltage                  | [V]     | N x M | Double |
% ------------------------------------------------------------------------
% | Vo       | Output voltage                 | [V]     | N x M | Double |
% ------------------------------------------------------------------------
% | Io       | Output current                 | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | fsw      | Switching frequency            | [Hz]    | N x M | Double |
% ------------------------------------------------------------------------
% | inductor | Inductor name                  | [-]     |       | Char   |
% ------------------------------------------------------------------------
% | diode    | Diode name                     | [-]     |       | Char   |
% ------------------------------------------------------------------------
% | switch_  | Switch name                    | [-]     |       | Char   |
% ------------------------------------------------------------------------
% | eta      | Efficiency estimate            | [-]     | 1     | Double |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name     | Description                    | Unit    | Size  | Type   |
% ========================================================================
% | Ploss    | Total power loss               | [W]     | N x M | Double |
% ------------------------------------------------------------------------
% | Pd       | Power loss in the diode        | [W]     | N x M | Double |
% -----------------------------------------------------------------------
% | Ps       | Power loss in the switch       | [W]     | N x M | Double |
% -----------------------------------------------------------------------
% | PLcond   | Inductor copper loss           | [W]     | N x M | Double |
% ------------------------------------------------------------------------
% | PLcore   | Inductor core loss             | [W]     | N x M | Double |
% ------------------------------------------------------------------------
% | Pssw     | Switching loss in the switch   | [W]     | N x M | Double |
% ------------------------------------------------------------------------
% | mode     | Converter operating mode       | [-]     | N x M | Logic  |
% ------------------------------------------------------------------------
function [Ploss, Pd, Ps, PLcond, PLcore, Pssw, mode] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_, eta)
    if(nargin == 7) % Calculate iteratively
        Niter = 0;
        eta  = 1;          % Initial efficiency estimate
        while(1)
            [Ploss, Pd, Ps, PLcond, PLcore, Pssw, mode] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_, eta);

            Po = Vo .* Io;
            Pi = Po + Ploss;
            
            eta_  = eta;      % Save previous estimate
            eta   = Po ./ Pi; % Calculate efficiency from losses
            Niter = Niter + 1;
            if(all(all(abs(eta - eta_) < 0.1/100)) || Niter > 10)
                break;
            end
        end
        
    elseif(nargin == 8) % Calculate using efficiency estimate
        [Ploss, Pd, Ps, PLcond, PLcore, Pssw, mode] = calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_, eta);
    else
        error('Incorrect number of input arguments')
    end
end

function [Ploss, Pd, Ps, PLcond, PLcore, Pssw, mode] = calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_, eta)
    %% Load component data
    [Rson, tson, tsoff, Cso] = f_load_switch(switch_);
    [Rdon, Vdsat, Cdo]       = f_load_diode(diode);
    [L, RLdc, Rcore0, Rac0]        = f_load_inductor(inductor);
    RLcore = Rcore0 * sqrt(fsw);    % Frequency-dependent core loss model
    Rac    = Rac0 * sqrt(fsw);
    
    %% Calculate operating point & rms currents
    [Iav, Iripp, D, d, mode]     = f_calc_op(Vi, Vo, Io, L, fsw, eta);
    [Isrms, Idrms, Ilrms, Idavg] = f_calc_rms(Iav, Iripp, D, d, mode);
    
    %% Conduction losses
    Pdcond = Rdon * Idrms.^2 + Vdsat .* Idavg;
    Pscond = Rson * Isrms.^2;

    PLcond = (RLdc + Rac) .* Ilrms.^2;
    PLcore = (Vi.^2 .* D + (Vi - Vo).^2 .* d) ./ RLcore;
    
    %% Switching losses
    Vsoff = Vo;                 % Switch turn-off voltage
    Vson  = Vo .* (1 - mode)... % Switch turn-on voltage CCM
            + Vi .* mode;       % Switch turn-on voltage DCM
    Isoff = Iav + 0.5 * Iripp;  % Switch turn-off current
    Ison  = (Iav - 0.5 * Iripp) .* (1 - mode)... % Switch turn-on current CCM
            + (0) .* mode;                       % Switch turn-on current DCM
        
    % Reverse recovery not modeled: Schottky diodes do not have this
    % Gate charging loss not modeled: no parameters available from datasheet
    Pssw_ton  = 0.5 * Vson .* Ison .* tson .* fsw;
    Pssw_toff = 0.5 * Vsoff .* Isoff .* tsoff .* fsw;    
    Pssw_co = 0.5 * (Cdo + Cso) * Vo.^2 .* fsw .* (1 - mode)...
           + (0.5 * (Cdo * (Vo - Vi).^2 + Cso * Vi.^2) .* fsw) .* mode;
    Pssw = Pssw_ton + Pssw_toff + Pssw_co;
       
    %% IC losses
    Pic = 0.0 * Io;     % Balancing
    
    %% Total losses
    Ploss = Pdcond + Pscond + PLcond + PLcore + Pic + Pssw;
    Ps = Pscond + Pssw;
    Pd = Pdcond;
end