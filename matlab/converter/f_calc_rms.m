% F_CALC_RMS Calculate RMS and average component currents
%   from operating point information.
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   13/05/2021
%
%  [Isrms, Idrms, Ilrms, Idavg] = f_calc_rms(Iav, Iripp, D, d, mode);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | Iav    | Average inductor current         | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | Iripp  | Inductor current ripple          | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | D      | Switch duty cycle                | [-]     | N x M | Double |
% ------------------------------------------------------------------------
% | d      | Diode duty cycle                 | [-]     | N x M | Double |
% ------------------------------------------------------------------------
% | mode   | Operation in DCM mode            | [-]     | N x M | Logic  |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | Isrms  | RMS switch current               | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | Idrms  | RMS diode current                | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | Ilrms  | RMS inductor current             | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | Idavg  | Average diode current            | [A]     | N x M | Double |
% ------------------------------------------------------------------------
function [Isrms, Idrms, Ilrms, Idavg] = f_calc_rms(Iav, Iripp, D, d, mode)
    % Continuous conduction mode
    Imin      = Iav - 0.5 * Iripp;
    Isrms_ccm = sqrt((Imin.^2 + Imin.*Iripp + Iripp.^2 / 3) .* D);
    Idrms_ccm = sqrt((Imin.^2 + Imin.*Iripp + Iripp.^2 / 3) .* (1 - D));
    Idavg_ccm = Iav .* (1 - D);
    
    % Discontinuous conduction mode
    Isrms_dcm = sqrt(D / 3) .* Iripp;
    Idrms_dcm = sqrt(d / 3) .* Iripp;
    Idavg_dcm = 0.5 * Iripp .* d;
    
    % Select correct mode
    Isrms = Isrms_ccm .* (1 - mode) + Isrms_dcm .* mode;
    Idrms = Idrms_ccm .* (1 - mode) + Idrms_dcm .* mode;
    Idavg = Idavg_ccm .* (1 - mode) + Idavg_dcm .* mode;
    Ilrms = sqrt(Idrms.^2 + Isrms.^2);
end