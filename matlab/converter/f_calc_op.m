% F_CALC_OP Calculate boost converter operating point
%   from input and output power, inductor value and switching frequency.
%  Author: Gijs Lagerweij, Charlotte De Jonghe
%  Date:   13/05/2021
%
%  [Iav, Iripp, D, d, DCM] = f_calc_op(Vi, Vo, Io, L, fsw, eta);
%
% Inputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | Vi     | Input voltage                    | [V]     | N x M | Double |
% ------------------------------------------------------------------------
% | Vo     | Output voltage                   | [V]     | N x M | Double |
% ------------------------------------------------------------------------
% | Io     | Output current                   | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | L      | Inductance                       | [H]     | N x M | Double |
% ------------------------------------------------------------------------
% | fsw    | Switching frequency              | [Hz]    | N x M | Double |
% ------------------------------------------------------------------------
% | eta    | Efficiency estimate              | [-]     | 1     | Double |
% ------------------------------------------------------------------------
%
% Outputs:
% ------------------------------------------------------------------------
% | Name   | Description                      | Unit    | Size  | Type   |
% ========================================================================
% | Iav    | Average inductor current         | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | Iripp  | Inductor current ripple          | [A]     | N x M | Double |
% ------------------------------------------------------------------------
% | D      | Switch duty cycle                | [-]     | N x M | Double |
% ------------------------------------------------------------------------
% | d      | Diode duty cycle                 | [-]     | N x M | Double |
% ------------------------------------------------------------------------
% | DCM    | Operation in DCM mode            | [-]     | N x M | Logic  |
% ------------------------------------------------------------------------
function [Iav, Iripp, D, d, DCM] = f_calc_op(Vi, Vo, Io, L, fsw, eta)
    D   = 1 - eta .* Vi ./ Vo;              % CCM duty cycle
    Iav = Io ./ (1 - D);                    % Average inductor current
    
    K     = 2 * L .* fsw ./ (Vo ./ Io);     % K factor
    Kcrit = D .* (1 - D).^2;                % Critical K for boost converter
    
    DCM = K < Kcrit;                        % Check if operation in DCM
    D   = D .* (1 - DCM)...                 % Duty cycle
        + sqrt(K ./ eta .* (Vo ./ Vi - 1) .* Vo ./ Vi) .* DCM;
    Iripp = Vi .* D ./ (L .* fsw);          % Ripple current
    d     = (1 - D) .* (1 - DCM)...         % Diode duty cycle
          + Iripp .* L .* fsw ./ (Vo - Vi) .* DCM;
end