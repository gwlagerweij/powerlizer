%%
Vi_ltspice = [12.5, 15, 18, 21];
%Pi_ltspice = [4.7554, 4.7443, 4.7282, 4.7152];
Pi_ltspice = [4.7537, 4.7403, 4.7282, 4.7069];

%%
Vi  = linspace(12.5, 21, 100);
Vo  = 36.67;
Io  = 120e-3;
fsw = 750e3;

inductor = 'XGL4030-123';
diode    = 'PMEG6010CEH';
switch_  = 'LT3599';

%%
[Ploss] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_);
Ploss = Ploss + 100e-3 + 0.77 * Io;

Po = (Vo - 0.77) .* Io;
Pi = Po + Ploss;
eta = Po ./ Pi * 100;

Ploss_ltspice = Pi_ltspice - Po;
eta_ltspice = Po ./ Pi_ltspice * 100;

Ploss_ltint = interp1(Vi_ltspice, Ploss_ltspice, Vi);
err = (Ploss - Ploss_ltint) ./ Ploss_ltint * 100;

%%
figure(1)
hold off
plot(Vi, Ploss * 1e3, 'k')
hold on
plot(Vi_ltspice, Ploss_ltspice * 1e3, 'kx--');

grid on
%grid minor

xlabel('Input Voltage [V]')
ylabel('Total Power Loss [mW]')

legend('MATLAB Model', 'LTspice Model', 'location', 'southwest')

figure(2)
hold off
plot(Vi, eta, 'k')
hold on
plot(Vi_ltspice, eta_ltspice, 'kx--');

grid on

xlabel('Input Voltage [V]')
ylabel('Efficiency [%]')
legend('MATLAB Model', 'LTspice Model', 'location', 'southeast')

figure(3)
hold on
plot(Vi, err, 'k')

grid on
xlabel('Input Voltage [V]')
ylabel('Relative Error [%]')

spread(3, 2)