%% Operating point
Vi  = 12.5;
Vo  = 39;
Io  = 120e-3;
fsw = 750e3;

diodes = {'MBR160', 'MBRS360', 'PMEG6010CEH', 'RB160M-60', 'RB578VYM100', 'DFLS160'};

inductor = 'LPS8045-104'; 'XGL4030-123';
switch_  = 'LT3599';

%%
Pd = nan(1, length(diodes));
Ps = nan(1, length(diodes));

for i = 1:length(diodes)
    diode = diodes{i};
    [~, Pd(i), Ps(i)] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_);
end

%%
figure(1);

tag  = categorical(diodes);
data = [Pd; Ps]';
b = bar(tag, data * 1e3, 'stacked');

ylabel('Power [mW]')

legend('Diode', 'Switch')

grid on