%%
Ii_proto = [391.5, 326.3, 273.5, 233.3] * 1e-3;
Ii_unc   = Ii_proto * 0.01 + 7 * 0.1e-3;

Vi_proto = [12.5, 15, 18, 21];
Vi_unc   = Vi_proto * 0.009 + 4 * 0.01;

Pi_proto = Vi_proto .* Ii_proto;
Pi_unc   = Pi_proto .* sqrt((Ii_unc ./ Ii_proto).^2 + (Vi_unc ./ Vi_proto).^2);

%%
Vi = linspace(12.5, 21, 100);
Po = (33.7 + 33.9 + 34.0 + 34.0) * 30e-3;
Io = 120e-3;
Vo = 34.8;

fsw = 765e3;

inductor = 'LPS4018-103';
diode    = 'PMEG6010CEH';
switch_  = 'LT3599';

%%
[Ploss] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_);
Ploss = Ploss + 380e-3 + 1.0 * Io;
Pi = Po + Ploss;
eta = Po ./ Pi * 100;

Ploss_proto = Pi_proto - Po;
eta_proto = Po ./ Pi_proto * 100;

eta_unc = eta_proto - [Po ./ (Pi_proto - Pi_unc) * 100; Po ./ (Pi_proto + Pi_unc) * 100];
eta_unc2 = Pi_unc .* Po ./ (Pi_proto.^2) * 100;

%%
figure(1)
hold off
plot(Vi, Ploss * 1e3, 'k')
hold on
%plot(Vi_proto, Ploss_proto * 1e3, 'kx--');
errorbar(Vi_proto, Ploss_proto * 1e3, Pi_unc * 1e3, 'kx--');

grid on
%grid minor

xlabel('Input Voltage [V]')
ylabel('Total Power Loss [mW]')

legend('MATLAB Model', 'Prototype', 'location', 'southwest')

figure(2)
hold off
plot(Vi, eta, 'k')
hold on
%plot(Vi_proto, eta_proto, 'kx--');
errorbar(Vi_proto, eta_proto, eta_unc2, 'kx--');

grid on

xlabel('Input Voltage [V]')
ylabel('Efficiency [%]')
legend('MATLAB Model', 'Prototype', 'location', 'northwest')

spread(3, 2)