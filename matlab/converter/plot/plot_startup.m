t  = startup21v(1:5:end, 1);
Vo = startup21v(1:5:end, 2);
Vi = startup21v(1:5:end, 3);
Io = startup21v(1:5:end, 4);

t  = t(2:end);
Vo = Vo(2:end);
Vi = Vi(2:end);
Io = Io(2:end);

%% Plot
figure(1);
tl = tiledlayout(2, 1);
tl.TileSpacing = 'compact';
tl.Padding = 'compact';

nexttile(1)
plot(t * 1e3, Vo, 'k');

ylabel('Output Voltage [V]')

xlim([0.5, 2])
grid on

nexttile(2)
plot(t * 1e3, Io * 1e3, 'k');

ylabel('Output Current [mA]')

xlim([0.5, 2])
ylim([0, 130])
grid on

xlabel(tl, 'Time [ms]')
tl.XLabel.FontSize = 9.9;

ax1 = nexttile(1);
ax2 = nexttile(2);
%labelx1 = ax1.YLabel.Position(1);
labelx2 = ax2.YLabel.Position(1);
ax1.YLabel.Position(1) = labelx2;