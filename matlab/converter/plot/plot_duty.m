%%
Vi  = linspace(12.5, 21, 100)';
Vo  = 39;
Io  = 120e-3;

Lfsw = 65;
L    = 120e-6;
fsw  = 1e6;

%%
[Iav, Iripp, D, d, mode] = f_calc_op(Vi, Vo, Io, L, fsw);

%%
figure(1); clf
hold on
plot(Vi, D)
plot(Vi, d)
plot(Vi, D + d)

legend('D', '\delta', 'D + \delta')

figure(2); clf
hold on
plot(Vi, Iav)
plot(Vi, Iripp)

legend('I_{av}', '\Deltai_L')

spread(3, 2)