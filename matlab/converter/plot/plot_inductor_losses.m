%% Operating point
Vi  = 12.5;
Vo  = 37;
Io  = 120e-3;

diode   = 'PMEG6010CEH';
switch_ = 'LT3599';

inductors_dcm = {'LPS4018-103', 'LPS5015-103', 'XGL4030-103', 'XGL4030-123', 'XGL4020-682'};
inductors_ccm = {'LPS8045-104', 'MSS1038-124', 'MSS1038-224', 'LPS6235-683', 'LPS6235-563', 'LPS4018-683'};
fsw_dcm       = [1140e3, 1140e3, 1140e3, 950e3, 1680e3];
fsw_ccm       = [750e3, 625e3, 340e3, 1100e3, 1340e3, 1100e3];

inductors = inductors_ccm;
fsw       = fsw_ccm;

%%
PLcond = nan(1, length(inductors));
PLcore = nan(1, length(inductors));
names  = {};

for i = 1:length(inductors)
    inductor = inductors{i}; 
    [~, ~, ~, PLcond(i), PLcore(i)] = f_calc_loss(Vi, Vo, Io, fsw(i), inductor, diode, switch_);
end

%%
figure(1);

tag  = categorical(inductors);
data = [PLcond; PLcore]';
b = bar(tag, data * 1e3, 'stacked');

ylabel('Power [mW]')
legend('Copper Loss', 'Core Loss')
grid on