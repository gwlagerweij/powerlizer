%%
Vi  = [12:3:21]';
Vo  = 37;
Io  = 120e-3;
fsw = linspace(200e3, 2.1e6, 100);

%%
inductor = 'LPS8045-104';
diode    = 'PMEG6010CEH'; 
switch_  = 'LT3599';
mode_set = 0;

[Ploss, Pd, Ps, PLcond, PLcore, Psw, mode] = f_calc_loss(Vi, Vo, Io, fsw, inductor, diode, switch_);

idx = logical(mode_set - mode);
Ploss(idx) = nan;

%%
figure(1);
plot(fsw * 1e-3, Ploss * 1e3)

grid on

xlabel('Switching Frequency [kHz]')
ylabel('Total Power Loss [mW]')

leg = sprintf('V_{in} = %.0f Vend', Vi);
leg = split(leg, 'end');
leg = leg(1:end-1);
legend(leg, 'location', 'southeast')

xlim([min(fsw), max(fsw)] * 1e-3)