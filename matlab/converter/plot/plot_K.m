D   = linspace(0, 1, 100);

Vi  = 21;
Vo  = Vi ./ (1 - D);
Io  = 120e-3;
L   = 10e-6;
fsw = 1e6;

K     = 2 * L .* fsw ./ (Vo ./ Io);     % K factor
Kcrit = D .* (1 - D).^2;                % Critical K for boost converter

%%
figure(1); clf
hold on
plot(D, K, 'k')
plot(D, Kcrit, 'r')

xlabel('Duty Cycle [-]')
ylabel('K Factor')
legend('K', 'K_{crit}')
