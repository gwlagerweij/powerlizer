Rz = 20e3;
Cz = 2.2e-9;
Cc = 100e-12;

f = logspace(2, 7, 100);
s = 1j * 2 * pi * f;

Z    = 1 ./ (s * (Cz + Cc)) .* (1 + s .* Rz * Cz) ./ (1 + s .* (Cc * Cz * Rz) ./ (Cc + Cz));
Zmag = 20 * log10(abs(Z));
Zph  = angle(Z) / pi * 180;

fz = 1 / (2 * pi * Rz * Cz);
fp = (Cc + Cz) / (2 * pi * Cc * Cz * Rz);

%% Plot
figure(1);
yyaxis left
semilogx(f, Zmag, 'k')
ylabel('Magnitude [dB]')

yticks([20 * log10(Rz/10), 20 * log10(Rz), 20 * log10(Rz * 10)])
yticklabels({'0.1 R_z', 'R_z', '10 R_z'})

a = gca;
a.YColor = [0, 0, 0];

yyaxis right
semilogx(f, Zph, 'k--')
ylabel('Phase [deg]')
ylim([-90, 0])
yticks([-90, -60, -30, 0])

a = gca;
a.YColor = [0, 0, 0];

xlabel('Frequency [Hz]')

xticks([fz fp])
xticklabels({'f_z', 'f_p'})

grid on

legend('Magnitude', 'Phase')