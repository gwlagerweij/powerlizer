t    = shutdown21v(1:end, 1);
Vled = shutdown21v(1:end, 3);
Vcmd = shutdown21v(1:end, 5);
Io   = shutdown21v(1:end, 6);

t0 = 4.5e-3;
t  = t - t0;

%% Plot
figure(1);
tl = tiledlayout(2, 1);
tl.TileSpacing = 'compact';
tl.Padding = 'compact';

ax1 = nexttile(1);

yyaxis left
plot(t * 1e6, Vcmd / 5, 'k');
ylabel('Shutdown Command')
ylim([0, 1.25])

yyaxis right
plot(t * 1e6, Io * 1e3, 'b');
ylabel('Output Current [mA]')
ylim([0, 130])

xlim([-0.5, 3])
grid on

ax1.YAxis(1).Color = [0,0,0];
ax1.YAxis(2).Color = [0,0,0];

nexttile(2)
plot(t * 1e6, Vled, 'k');

ylabel('LED Pin Voltage [V]')

xlim([-0.5, 3])
ylim([0, 15])
grid on

ax1 = nexttile(1);
ax2 = nexttile(2);
%labelx1 = ax1.YLabel.Position(1);
labelx2 = ax2.YLabel.Position(1);
ax1.YAxis(1).Label.Position(1) = labelx2;

xlabel(tl, ['Time [', char(181), 's]'])
tl.XLabel.FontSize = 9.9;

nexttile(1);
legend('Command', 'Current', 'Location', 'East')