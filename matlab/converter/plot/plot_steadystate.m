%%
t   = steady21v(end - 1250:end, 1);
vsw = steady21v(end - 1250:end, 2);
il  = steady21v(end - 1250:end, 3);

t0 = 7.8055e-3;
t = t - t0;

%%
figure(1);
tl = tiledlayout(2, 1);
%tl.TileSpacing = 'compact';
tl.Padding = 'compact';

nexttile(1)
plot(t * 1e6, vsw, 'k');

ylabel('Switch Voltage [V]')
xlim([0, 4])
xticks([0, 1, 2, 3, 4])
xticklabels({'t_0', '+1', '+2', '+3', '+4'})
grid on

nexttile(2)
plot(t * 1e6, il * 1e3, 'k');

ylabel('Inductor Current [mA]')

ylim([-100, 900])
xlim([0, 4])
xticks([0, 1, 2, 3, 4])
xticklabels({'t_0', '+1', '+2', '+3', '+4'})
grid on

xlabel(tl, ['Time [', char(181), 's]'])
tl.XLabel.FontSize = 9.9;

ax1 = nexttile(1);
ax2 = nexttile(2);
%labelx1 = ax1.YLabel.Position(1);
labelx2 = ax2.YLabel.Position(1);
ax1.YLabel.Position(1) = labelx2;